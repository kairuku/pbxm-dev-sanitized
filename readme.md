This is a *sanitized* version of a legacy, minimal, procedural LEMP app used in production at a non-developer job. Its purpose was primarily to create purchase orders and generate shelf tags in a small wine and spirits shop.

## Screenshots
Click thumbnail images below for larger view

### Web UI
[![Screenshot of the web UI of the app.](_repo_meta/web-ui.tn-900.png)](https://bitbucket.org/kairuku/pbxm-dev-sanitized/raw/f86a8706db5806bd8cb95a21c840cfd2d88b5ff2/_repo_meta/web-ui.png)

### DigitalOcean Console
[![Screenshot of the DigitalOcean console.](_repo_meta/do-console.tn-900.png)](https://bitbucket.org/kairuku/pbxm-dev-sanitized/raw/f86a8706db5806bd8cb95a21c840cfd2d88b5ff2/_repo_meta/do-console.png)

At the time, DigitalOcean offered droplets running Arch Linux (the Arch Linux website itself runs on Arch, in spite of the FUD surrounding using a rolling release distro for servers)