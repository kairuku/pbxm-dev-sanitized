<?php

// introduce remote kill switch here with cwd conditional, ie, it's only needed on xtal

error_reporting (E_ALL & ~E_NOTICE & ~E_STRICT);

// no more imports file
require_once('../config/constants.cfg.php');
require_once('../config/db-mysql.cfg.php');
require_once('../config/routes.cfg.php');

?>
