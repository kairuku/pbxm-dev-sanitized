<?php

// need to also add a conditional where only vendors who have orders are displayed in the page
// header, AND that section should be collapsible in case a vendor is actually looking at the page

//-------------------------------------------------------------------------

// LAST WEEK TEMP (BEN ARNOLD mainly)
$sql_of_balst = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `childVendor`=58
                ) dt;";

$result_of_balst = mysql_query($sql_of_balst) or die(mysql_error());
$number_raw_of_balst = mysql_result($result_of_balst, 0);
// $number_of_balst = number_format($number_raw_of_balst, 2, '.',',');
$number_of_balst = round($number_raw_of_balst);


//-------------------------------------------------------------------------


// select sum (order_substotal_projected) as total from 1_products;
$sql_op = "select sum(order_subtotal_projected) from 1_products;";
$result_op = mysql_query($sql_op) or die(mysql_error());
$number_raw_op = mysql_result($result_op, 0);
$number_op = number_format($number_raw_op, 2, '.',',');
//echo '<p class="attn">Current order subtotal projected
// (liquor and wine combined) is: $' . $number_op . "</p>";

//-------------------------------------------------------------------------
// don't use SELECT DISTINCT in the subqueries, the totals are wrong

// ORDER FINAL COMBINED

$sql_of = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                ) dt;";

$result_of = mysql_query($sql_of) or die(mysql_error());
$number_raw_of = mysql_result($result_of, 0);
// $number_of = number_format($number_raw_of, 2, '.',',');
$number_of = round($number_raw_of, 2);

// $number_of_cw  = $number_of - $number_of_balst; 
$number_of_cw_nr  = $number_raw_of - $number_raw_of_balst; 
$number_of_cw = round($number_of_cw_nr,2);


    echo 'AREA 49 -- ' . $result_of . '<br>';
    echo 'AREA 50 -- ' . $number_raw_of. '<br>';
    echo 'AREA 51 -- ' . $number_of . '<br>';
    echo 'AREA 52 -- ' . $number_raw_of_balst . '<br>';
    echo 'AREA 53 -- ' . $number_of_cw_nr . '<br>';
    echo 'AREA 54 -- ' . $number_of_cw;



//-------------------------------------------------------------------------
// liquor only
$sql_ofl = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `order_group`='liquor'
                ) dt;";

$result_ofl = mysql_query($sql_ofl) or die(mysql_error());
$number_raw_ofl = mysql_result($result_ofl, 0);
$number_ofl = number_format($number_raw_ofl, 2, '.',',');
//-------------------------------------------------------------------------
// wine only
$sql_ofw = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `order_group`='wine'
                ) dt;";

$result_ofw = mysql_query($sql_ofw) or die(mysql_error());
$number_raw_ofw = mysql_result($result_ofw, 0);
$number_ofw = number_format($number_raw_ofw, 2, '.',',');
//-------------------------------------------------------------------------
// NOW DO SPECIFIC VENDORS
//-------------------------------------------------------------------------
// ALEPH
$sql_of_ap = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `childVendor`=19
                ) dt;";

$result_of_ap = mysql_query($sql_of_ap) or die(mysql_error());
$number_raw_of_ap = mysql_result($result_of_ap, 0);
$number_of_ap = number_format($number_raw_of_ap, 2, '.',',');
//-------------------------------------------------------------------------
// BEN ARNOLD
$sql_of_ba = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `company_temp`='benarnold'
                ) dt;";

$result_of_ba = mysql_query($sql_of_ba) or die(mysql_error());
$number_raw_of_ba = mysql_result($result_of_ba, 0);
$number_of_ba = number_format($number_raw_of_ba, 2, '.',',');

//-------------------------------------------------------------------------

// REPUBLIC
$sql_of_rn = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `company_temp`='republic'
                ) dt;";

$result_of_rn = mysql_query($sql_of_rn) or die(mysql_error());
$number_raw_of_rn = mysql_result($result_of_rn, 0);
$number_of_rn = number_format($number_raw_of_rn, 2, '.',',');
//-------------------------------------------------------------------------
// SOUTHERN
$sql_of_st = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `company_temp`='southern'
                ) dt;";

$result_of_st = mysql_query($sql_of_st) or die(mysql_error());
$number_raw_of_st = mysql_result($result_of_st, 0);
$number_of_st = number_format($number_raw_of_st, 2, '.',',');
//-------------------------------------------------------------------------
// ADVINTAGE
$sql_of_av = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `childVendor`=25
                ) dt;";

$result_of_av = mysql_query($sql_of_av) or die(mysql_error());
$number_raw_of_av = mysql_result($result_of_av, 0);
$number_of_av = number_format($number_raw_of_av, 2, '.',',');
//-------------------------------------------------------------------------
// COUNTRY VINTNER
$sql_of_cv = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `childVendor`=1
                ) dt;";

$result_of_cv = mysql_query($sql_of_cv) or die(mysql_error());
$number_raw_of_cv = mysql_result($result_of_cv, 0);
$number_of_cv = number_format($number_raw_of_cv, 2, '.',',');
//-------------------------------------------------------------------------
// GRAPEVINE
$sql_of_gp = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `childVendor`=21
                ) dt;";

$result_of_gp = mysql_query($sql_of_gp) or die(mysql_error());
$number_raw_of_gp = mysql_result($result_of_gp, 0);
$number_of_gp = number_format($number_raw_of_gp, 2, '.',',');
//-------------------------------------------------------------------------
// GRASSROOTS
$sql_of_gs = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `childVendor`=3
                ) dt;";

$result_of_gs = mysql_query($sql_of_gs) or die(mysql_error());
$number_raw_of_gs = mysql_result($result_of_gs, 0);
$number_of_gs = number_format($number_raw_of_gs, 2, '.',',');
//-------------------------------------------------------------------------
// DUMMY
$sql_of_dm = "SELECT SUM(`subb`)
         FROM (
                SELECT
                    `order_subtotal_projected` as `subb`
                FROM `1_products`
                WHERE `order_flag_final`='y'
                AND `childVendor`=54
                ) dt;";

$result_of_dm = mysql_query($sql_of_dm) or die(mysql_error());
$number_raw_of_dm = mysql_result($result_of_dm, 0);
$number_of_dm = number_format($number_raw_of_dm, 2, '.',',');
//-------------------------------------------------------------------------
// need something besides remainder ... e.g, actual (past) vs still to come
$remainder_raw = ($number_raw_of - $number_raw_of_dm);
$remainder = number_format($remainder_raw, 2, '.',',');
//-------------------------------------------------------------------------
$bank_raw = (0 - $remainder_raw);
$bank = number_format($bank_raw, 2, '.',',');
//-------------------------------------------------------------------------

// echo '<div class="attn">Current projected total order cost (combined, with FINAL flag): $' . $number_of .
echo '<div class="attn">Current projected total order cost (combined, with FINAL flag): $' . $number_of_cw .
// collapse and expand all below via javascript
        '<span style="float:right;"><a id="displayText" href="javascript:toggle();">expand</a></span>' .
        '<div id="toggleText" style="display: none">' .
        'Liquor Subgroup=' . $number_ofl . ';&nbsp;&nbsp;Wine Subgroup=' . $number_ofw .
    '<hr>' .
    '<br>Advintage=' . $number_of_av .
    '<br>Country Vintner=' . $number_of_cv .
    '<br>Grapevine=' . $number_of_gp .
    '<br>Grassroots=' . $number_of_gs .
    '<hr>' .
    '<br>Aleph=' . $number_of_ap .
    '<br>Ben Arnold=' . $number_of_ba .    
    '<br>Republic=' . $number_of_rn .
    '<br>Southern=' . $number_of_st .
    '<br>LAST WEEK TEMP =' . $number_of_balst .
    '<hr>' .
    '<br>division 1 (placeholder) =' . 'variable1' .
    ';&nbsp;&nbsp;division 2 (placeholder) =' . 'variable2' .
 //    ';&nbsp;&nbsp;dummy=' . $number_of_dm .
 //   ';&nbsp;&nbsp;remainder=' . $remainder . 
 //   ';&nbsp;&nbsp;bank after=' . $bank . 
    '</div></div>';

//-------------------------------------------------------------------------

/*

SELECT   COUNT(*)
FROM    (SELECT   DISTINCT
                  k.kingdom_id
         ,        kki.kingdom_name
         ,        kki.population
         FROM     kingdom_knight_import kki LEFT JOIN kingdom k
         ON       kki.kingdom_name = k.kingdom_name
         AND      kki.population = k.population) dt;

*/
?>