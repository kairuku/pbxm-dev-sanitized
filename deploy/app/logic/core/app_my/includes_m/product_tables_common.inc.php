<?php
//------------------------------------------------------------------------------
/*

	DEPRECATED?
	// display what the person searched for
		echo "<p>" . $numrows . " unpaginated results found</p>";
	// apparently I don't need to concatenate WITHIN quotes in an echo statement
		echo '<table class="scrollable">
*/

//------------------------------------------------------------------------------
// display header row

/*
	// echo '<div>
	<th>Proposed</th>
	<th>Print Tag</th>
	<th>Clone</th>
	<th>Archive</th>
*/

echo '
<div style="padding-top:2em;">
<span style="float:right;">
Click column headers to sort; if you resize the window, hit F5.
</span>
    <table id="liqfin">
        <thead>
            <tr>
                <th class="freeze">Product</th>
                <th>Vol</th>
                <th>Pack</th>
                <th>Target</th>
                <th>Landed</th>
                <th>Retail</th>

                <th>Subtotal</th>
                <th>Order Status</th>

                <th>Qty (cs)</th>
                <th>Qty (bt)</th>

                <th>Deal</th>
                <th>Order By</th>

                <th>Credit Amount</th>
                <th>Has Notes?</th>



<th>ALLOCATED</th>

                <th>Vendor</th>
                <th>Company Temp</th> <!-- deprecated -->
                <th>Rep</th> <!-- deprecated -->
                <th>Phone</th> <!-- deprecated -->
                <th>Comments</th> <!-- deprecated -->
                <th>Barcode</th>
                <th>PK</th>
            </tr>
        </thead>
        <tbody>
        </div>';

//------------------------------------------------------------------------------
// is it possible to pull this into a secondary loop so you can output rep phone numbers one time, instead of on every row?

// or just pull this out and put it in section sepaprate from the table, and independent of table sorting

// or add the phone to the totals section

//------------------------------------------------------------------------------
// might put this loop in another file to re-use

// loop through content rows
while ($row = mysql_fetch_array($result)) {
    $productName    = $row["productName"];
    $size           = $row["size"];
    $deal           = $row["deal"];
    
    $qty_cs            = $row["order_qty_cs"];
    $qty_bt            = $row["order_qty_bt"];


$status            = $row["status"];

    $order_by       = $row["order_by"];
    $subtotal       = $row["order_subtotal_projected"];
    $cost           = $row["cost"];
	$landed 		= $row["landed"];
    $retail         = $row["retail"];
    $pack           = $row["pack"];

    $vendor         = $row["vendor"];

    $company_temp	= $row["company_temp"];   // deprecated
    $rep            = $row["rep-full-name"];  // deprecated
    $phone          = $row["rep-phone"];  // deprecated
    $comments       = $row["orderComments"];  // deprecated

    $upc			= $row["upc"];

    $idItemNum      = $row["idItemNum"];
    

//------------------------------------------------------------------------------
// print rows to screen while looping
// for some reason, single quotes in the next echo don't work,
// apparently because PHP variables are involved?

/*
	<td id=\"tbl_ord_rep\">$vendor</td>
	echo "<tr class=\"$zstate\" >

	<td><a href='index.php?p=flag_set_order_proposed&amp;idItemNum=$idItemNum'>
		Proposed</a></td>
	<td><a href='index.php?p=flag_set_print_tag&amp;idItemNum=$idItemNum'>
		Print Tag</a></td>
	<td><a href=''>Clone</a></td>
	<td><a href=''>Archive</a></td>
*/

    echo "<tr>
    			<!--
    				<td>$count</td>
    			-->

            <td class=\"freeze\">
                <a href='fc.php?p=product_edit_form&amp;idItemNum=$idItemNum'>
                	$productName
                		</a></td>
	<td class=\"just_r\">$size</td>
           <td class=\"just_r\">$pack</td>
            <td class=\"just_r\">$cost</td>         
            <td class=\"just_r\">$landed</td>
            <td class=\"just_r\">$retail</td>            

            <td class=\"highlight_rj\">$subtotal</td>
            <td class=\"highlight_rj\">$status</td>

            <td class=\"just_r\">$qty_cs</td>
            <td class=\"just_r\">$qty_bt</td>

            <td>$deal</td>
            <td>$order_by</td>

            <td class=\"just_r\"> -- </td>
            <td> -- </td>
            <td> -- </td>

            <td>$vendor</td>
	<td>$company_temp</td>
            <td>$rep</td>
            <td>$phone</td>
            <td>$comments</td>
            <td>$upc</td>
            <td class=\"just_r\">$idItemNum</td>
        </tr>";
//------------------------------------------------------------------------------
/*        
	//deal with the row styling
	if($zstate == $zebra_on){
	    $zstate = $zebra_off;
	} else {
	    $zstate = $zebra_on;
	}

	// $count++ ;

*/
//------------------------------------------------------------------------------
      } // END WHILE
//------------------------------------------------------------------------------
echo "</tbody>
	</table>";
// echo "</tbody></table></div>";

/* echo "<p>Note: scrolling table only works in Firefox, Opera, Google Chrome,
Safari (or more generally, browsers using the Gecko, WebKit and
Presto <a href=\"http://en.wikipedia.org/wiki/Comparison_of_layout_engines_(XHTML)\" target=\"_blank\">layout engines</a>); it doesn't work in IE (imagine that)</p>"; */

//------------------------------------------------------------------------------
?>