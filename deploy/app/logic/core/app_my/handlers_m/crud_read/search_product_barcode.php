<?php
// this page expects to receive $_GET['terms']
// Display the search results if the form has been submitted.
if (isset($_GET['search_code']) && ($_GET['search_code'] != '') ) { // if isset

	// Get the search variable(S) from URL
	$search_code_raw = @$_GET['search_code'] ;
	$search_code_edited = trim($search_code_raw); //trim whitespace
	$search_code_edited = str_replace("%","\%",$search_code_edited); // escape percentage signs

	// check for an empty string and display a message.
	if ($search_code_edited == "")  {
		echo "<p>Please enter a search...</p>";
		exit;
	}

	// check for a search parameter
	if (!isset($search_code_raw))  {
	    echo "<p>We dont seem to have a search parameter!</p>";
	    exit;
	}

	// create query
	$query = "select * FROM view_search_products WHERE upc
	    		like \"%$search_code_edited%\" order by productName";

// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// common stuff begins here; the main differentiator is the actual query
	    		
	$numresults=mysql_query($query);
	$numrows=mysql_num_rows($numresults);

	// if we have no results, tell the user
	if ($numrows == 0){ // consider reversing the order of these conditions if it helps readability
	     echo "<p>Sorry, no results matched &quot;<strong>" . $search_term_raw . "</strong>&quot;</p>";
	}
	else { // else inner
	    $result = mysql_query($query) or die(mysql_error());

//------------------------------------------------------------------------------
require_once(URI_TABLES_M);

} // END ELSE
// common stuff ends
//------------------------------------------------------------------------------
} // end if isset

else { // Tell them to use the search form.
	echo '<p class="error">Please use the search form at the top of the window to search this site.</p>';
}
?>