<?php

$query = "select * from view_order_liquor_proposed";
$numresults=mysql_query($query);
$numrows=mysql_num_rows($numresults);
$empty = "<p>Sorry, we have no PROPOSED liquor order</p>";

// if we have no results, tell the user
if ($numrows == 0){
    echo $empty;
}
else { // get results
            $result = mysql_query($query) or die(mysql_error());
            // display header row
    echo '<div>
            <table id="liqfin">
                <thead>
                    <tr>
                        <th class="freeze">Product Name</th>
                        <th>Proposed?</th>
                        <th>Final?</th>
                        <th>Volume</th>
                        <th>Deal Type</th>
                        <th>Order By</th>
                        <th>Order Qty</th>
                        <th>Subtotal</th>
                        <th>Cost Target</th>
                        <th>Pack</th>                        
                        <th>Comments</th>
                        <th>Vendor</th>
                        <th>Rep</th>
                        <th>Phone</th>
                        <th>PK</th>
                    </tr>
                </thead>
                <tbody>';


            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $ofprop         = $row["order_flag_proposed"];
                    $offinal        = $row["order_flag_final"];
                    $idItemNum      = $row["idItemNum"];
                    $productName    = $row["productName"];
                    $size           = $row["size"];
                    $cost           = $row["cost"];
                    $retail         = $row["retail"];
                    $pack           = $row["pack"];
                    $deal           = $row["deal"];
                    $qty            = $row["order_qty_proposed"];
                    $order_by       = $row["order_by"];
                    $subtotal       = $row["order_subtotal_projected"];
                    $vendor         = $row["vendor"];
                    $rep            = $row["rep-full-name"];
                    $phone          = $row["rep-phone"];
                    $comments       = $row["orderComments"];

            // print rows to screen
            // for some reason, single quotes in the next echo don't work,
            // apparently because PHP variables are involved?


            echo "<tr>
                    <td class=\"freeze\">
                        <a href='index.php?p=product_edit_form&amp;idItemNum=$idItemNum'>$productName</a></td>
                    <td>$ofprop</td>
                    <td>$offinal</td>                    
                    <td class=\"just_r\">$size</td>
                    <td>$deal</td>
                    <td>$order_by</td>                    
                    <td class=\"just_r\">$qty</td>
                    <td class=\"highlight_rj\">$subtotal</td>
                    <td class=\"just_r\">$cost</td>
                    <td class=\"just_r\">$pack</td>                    
                    <td>$comments</td>
                    <td>$vendor</td>
                    <td>$rep</td>
                    <td>$phone</td>
                    <td class=\"just_r\">$idItemNum</td>
                </tr>";

            //deal with the row styling
            // if($zstate == $zebra_on){
            //    $zstate = $zebra_off;
            //} else {
            //    $zstate = $zebra_on;
            //}
                } // END WHILE

            echo "</tbody></table></div>";
        /* echo "<p>Note: scrolling table only works in Firefox, Opera, Google Chrome,
        Safari (or more generally, browsers using the Gecko, WebKit and
        Presto <a href=\"http://en.wikipedia.org/wiki/Comparison_of_layout_engines_(XHTML)\" target=\"_blank\">layout engines</a>); it doesn't work in IE (imagine that)</p>"; */
        } // END ELSE
?>
