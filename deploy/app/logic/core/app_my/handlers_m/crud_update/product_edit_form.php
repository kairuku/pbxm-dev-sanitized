<?php

/*
workflow (as of 2014 OCTOBER:
if adding fields to the form, start in this file after you've amended the database (and base view)
add the new field names to the initial query in this script (until a base view is created/updated)
OR in the SECOND query of this script if the first has been replaced with a base view for all select scripts
Then copy and paste the fields in in a comment block in the POST array section of the SUBMIT script
Then proceed to amend both relevant sections of the SUBMIT script, possibly running PHP lint on both
scripts pre-emptively.

ONLY NOW should you touch the HTML template(s) for the form.

NOW test in the browser, and if everything works do a Git commit (not necessarily a push though)

clean up indents or such now if needed and commit again

AND THEN realize the product ADDITION form is now out of sync and should be refactored as well

at some point the main product table or fields therein should likely be refactored into new tables, though that 
may or may not affect the length of this form
*/


// NEED TO EXTRACT COMMON FORM LOGIC INTO AN INCLUDE, COMMON FORM HTML INTO A LOCAL TEMPLATE



	// how do I get the search string here, and preserve it, to send on the processor,
	// and make a link back to the previous search results?


// should use a database view for the next querySizes, but I'm not sure how the id would be submitted
// from the GET array to the where clause in the external view

  $idItemNum = $_GET['idItemNum']; // GET, because this is coming from a link

/*
// this doesn't work
$author = mysql_query("
	SELECT * FROM (select @p1:='$idItemNum' p) parm , view_product_edit
");
*/

//view_prod_ed_form

// $author = sqlw_query("SELECT
// why don't i just SELECT * AT THIS POINT?!?!?!
$products_query = sqlw_query("SELECT
`productName`,
`public`,
`productNameMini`,
`upc`,
`company_temp`,
`childVendor`,
`childSize`,
`childCountry`,
`childOrderStatus`,
`pack`,
`cost`,
`retail`,
`retailWas`,
`retail_birsel`,
`last_birsel_add`,
`last_birsel_minus`,
`last_birsel_date`,
`birsel_note`,
`childBPS`,

`allocated`,
`tarik_seeks`,
`voos`,
`pre_markdown`,
`markdown`,
`clearance`,
`has_markdown_tag`,

`printTag`,
`printTalker`,
`order_group`,
`orderComments`,
`public`,
`dateFirstStocked`,
`order_by`,
`order_subtotal_projected`,
`bcf_base`,
`bcf`,
`order_qty_cs`,
`order_qty_bt`,
`landed`,
`childDeal`,
`order_status`,
`size`,
`childShelfSection`,
`flag_new_while_dg_gone`,
`flag_order_legacy`
FROM
-- this isn't clear, since the fields above aren't fully qualified
`1_products`, `2_sizes`, `2_countries`, `2_deals`, `2_order_status`,`2_brs_prc_sts`
WHERE
`idItemNum`='$idItemNum'
AND `1_products`.`childSize` = `2_sizes`.`id`
");


	// `childRep`,
	// `childVendorDiv`,

// if (!$author) {
  if (!$products_query) {
    exit('<p>Error fetching product details: ' .
        mysql_error() . '</p>');
  }

  // $author = sqlw_fetch_array($author);
  $author = sqlw_fetch_array($products_query);


// dropdown conditional variables initialization
// these are only used in this file, they are not submitted
  $childVendorExists 		= $author['childVendor'];
  $childSizeExists 			= $author['childSize'];
  $childDealExists 			= $author['childDeal'];
  $childOrderStatusExists 			= $author['childOrderStatus'];
  $childCountryExists 		= $author['childCountry'];
  $childBPSExists 			= $author['childBPS'];
  $childShelfSectionExists    = $author['childShelfSection'];
//$childVendorDivExists = $author['childVendorDiv'];
//$childRepExists = $author['childRep'];

// other variables
  $productName 				= $author['productName'];
  $upc 						= $author['upc'];
  $company_temp				= $author['company_temp'];
  $public 					= $author['public'];
  $productNameMini 			= $author['productNameMini'];
  $pack 					= $author['pack'];
  $cost 					= $author['cost'];
  $dateFirstStocked 		= $author['dateFirstStocked'];
  $retail 					= $author['retail'];
  $retailWas 				= $author['retailWas'];

  $retail_birsel			= $author['retail_birsel'];
  $birsel_pricing_status	= $author['birsel_pricing_status'];
  $last_birsel_add 			= $author['last_birsel_add'];
  $last_birsel_minus		= $author['last_birsel_minus'];
  $last_birsel_date 		= $author['last_birsel_date'];
  $birsel_note 				= $author['birsel_note'];

  $order_group              = $author['order_group'];
  $orderComments 			= $author['orderComments'];

  // 2014 october radio button addtions

  $allocated         = $author['allocated'];
  $tarik_seeks         = $author['tarik_seeks'];
  $voos         = $author['voos'];
  $pre_markdown         = $author['pre_markdown'];
  $markdown         = $author['markdown'];
  $clearance         = $author['clearance'];
  $has_markdown_tag         = $author['has_markdown_tag'];


  $printTag 				= $author['printTag'];
  $printTalker 				= $author['printTalker'];
  $public 					= $author['public'];
  $order_by 				= $author['order_by'];
  $order_subtotal_projected	= $author['order_subtotal_projected'];
  $bcf_base 				= $author['bcf_base'];
  $bcf 						= $author['bcf'];
  
$bcf 						= $author['bcf'];

  $order_qty_cs		= $author['order_qty_cs'];
  $order_qty_bt		= $author['order_qty_bt'];
  $landed	 				= $author['landed'];
  $size	 				    = $author['size'];
  $flag_new_while_dg_gone   = $author['flag_new_while_dg_gone'];
  $flag_order_legacy   = $author['flag_order_legacy'];

  // Convert special characters for safe use
  // as HTML attributes.
  $productName = htmlspecialchars($productName);
  $productNameMini = htmlspecialchars($productNameMini);
  //$cost = htmlspecialchars($cost);
  $orderComments = htmlspecialchars($orderComments);
  //$retail = htmlspecialchars($retail);        //use a different functionf for Doubles?
  //$retailWas = htmlspecialchars($retailWas);  //use a different functionf for Doubles?


// -----------------------------------------------------------
// make a variable for the checked attribute of ALL radio buttons ?
// or just have a set of echo statements for both options in each conditional?
	//$radio_checked = "checked";
	//$radio_unchecked = "";
	//$radio_checked_echo = $radio_unchecked;

// require_once (URI_TEMPLATE_GLOBAL . 'nav-menu-global.html');

require_once (URI_TEMPLATE_LOCAL . 'product-edit-form/product-edit-form-main-head.html');
require_once (URI_TEMPLATE_LOCAL . 'product-edit-form/product-edit-form-part-01-mid.html');
require_once (URI_TEMPLATE_LOCAL . 'product-edit-form/product-edit-form-part-02-ordering.html');
require_once (URI_TEMPLATE_LOCAL . 'product-edit-form/product-edit-form-main-foot.html');

?>
