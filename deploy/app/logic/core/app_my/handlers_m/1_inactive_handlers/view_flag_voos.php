<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
    // Need the BASE_URL, defined in the config file:
    require_once ('../../config.php');
    // Redirect to the index page:
    $url = BASE_URL . 'index.php?p=vfvoos';
    header ("Location: $url");
    exit;
}
// =============================================================================
// $query = "select * FROM view_search_order_final_flags_error";
$numresults=mysql_query($query);
$numrows=mysql_num_rows($numresults);
$empty = "<p>Sorry, we have vendor out-of-stocks to show</p>";

// if we have no results, tell the user
if ($numrows == 0){
    echo $empty;
}
else { // get results

/*
1_products.idItemNum,
1_products.order_by,
1_products.order_group,
1_products.childSize,
*/


            $result = mysql_query($query) or die(mysql_error());
            // display header row
            echo '<div class="table_container">
					<table class="scroll_table">
                    <thead class="fixed_thead"><tr>
                        <th>PK</th>
                        <th>ProductName</th>
                        <th>Size</th>
                        <th>Flag Proposed</th>
                        <th>Flag Final</th>
                         </tr></thead>
			<tbody class="scroll_tbody">';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $idItemNum           = $row["idItemNum"];
                    $productName         = $row["productName"];
                    $size                = $row["size"];
                    $order_flag_proposed = $row["order_flag_proposed"];
                    $order_flag_final    = $row["order_flag_final"];

                    // print rows to screen
		    // for some reason, single quotes in the next echo don't work,
		    // apparently because PHP variables are involved?
                    echo "<tr class=\"$zstate\" >
                            <td>$idItemNum</td>
                            <td><a href='index.php?p=product_edit_form&amp;idItemNum=$idItemNum'>$productName</a></td>
                            <td>$size</td>
                            <td>$order_flag_proposed</td>
                            <td>$order_flag_final</td>
                             </tr>";

			//deal with the row styling
			if($zstate == $zebra_on){
				$zstate = $zebra_off;
			} else {
				$zstate = $zebra_on;
			}
                } // END WHILE

            echo "</tbody></table></div>";
	    /* echo "<p>Note: scrolling table only works in Firefox, Opera, Google Chrome,
		Safari (or more generally, browsers using the Gecko, WebKit and
		Presto <a href=\"http://en.wikipedia.org/wiki/Comparison_of_layout_engines_(XHTML)\" target=\"_blank\">layout engines</a>); it doesn't work in IE (imagine that)</p>"; */
        } // END ELSE
?>
