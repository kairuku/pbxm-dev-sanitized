<?php
//  This page expects to receive $_GET['terms']
// =============================================================================

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
	// Need the BASE_URL, defined in the config file:
	require_once ('../../config.php');
	// Redirect to the index page:
	$url = BASE_URL . 'index.php?p=spn';

		// Pass along search terms?
		if (isset($_GET['search_term'])) {
			$url .= '&search_term=' . urlencode($_GET['search_term']);
		}

	header ("Location: $url");
	//echo "db inside conditional"; // this seems to get ignored if I access the script directly with a search term
	// at the same time, no echo's can come before the header statement, or the header will throw an error
	exit;

}
// =============================================================================

// Print a caption:Posts
//echo '<h2>Search Results</h2>';

// Display the search results if the form
// has been submitted.
if (isset($_GET['search_term']) && ($_GET['search_term'] != '') ) {
//if (isset($_GET['search_term']) && ($_GET['search_term'] != 'Search for a product ...') ) {

//-------------------------------------------------------------------------------------------------

	// Get the search variable(S) from URL
	    $s =   @$_GET['s'] ; // this crucial line was missing from original script!!!!!!!!!!!!!!!!!!!!!!!
	    $search_term_raw = @$_GET['search_term'] ;
	    $search_term_edited = trim($search_term_raw); //trim whitespace
	    $search_term_edited = str_replace("%","\%",$search_term_edited); // escape percentage signs

	    if(isset($_GET['limit_return'])){$limit_return=@$_GET['limit_return'];}
	    $limit_start=@$_GET['limit_start'] ;

	    // best thing I could probably do with apostrophes is not  use them in db
	    // and strip them out of $search_term_edited ?

	// check for an empty string and display a message.
	    if ($search_term_edited == "")  {
	      echo "<p>Please enter a search...</p>";
	      exit;  }
	// check for a search parameter
	    if (!isset($search_term_raw))  {
	      echo "<p>We dont seem to have a search parameter!</p>";
	      exit;  }

	// next line has to be included here, but can't be included in the edit module
	    mysql_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");
	    $query = "select * FROM view_search_products WHERE productName like \"%$search_term_edited%\" order by productName";
	    $numresults=mysql_query($query);
	    $numrows=mysql_num_rows($numresults);




	// rows to return -- this could be set by the user too
	      if (empty($sql_limit)) {
			$sql_limit=600; // default
	      }

	      if (empty($sql_offset)) {
			$sql_offset=0; // if it's set to one, single row results don't display
	      }
	    $page_current =$_REQUEST['page_current'];
	    	      if (empty($page_current)) {
			$page_current=1;
	      }


	// If we have no results, tell the user
	   if ($numrows == 0){ // consider reversing the order of these conditions. if it helps readability
	      echo "<p>Sorry, no results matched &quot;<strong>" . $search_term_raw . "</strong>&quot;</p>";
	    }
	    else {
			// set pagination variables -- from HTTP or by hand or both?
			// how many variables do you want to pass in URL, and how many do you want to compute ~here~?
			$row_total = $numrows;
			$row_start = $sql_offset;
				if ($row_start==0){$row_start=1;}
			$row_end = $sql_offset + $sql_limit;

			// have to get whole number for total pages before we can determine last page number
			$page_total = $row_total / $sql_limit;
			if (is_int($page_total)){
				$page_total=$page_total;
			} else {
				$page_total=intval($page_total) + 1;
			}

			// is the next variable redundant?
			$page_last = $page_total;

			// the first page cannot be changed, make it a constant
			//define PAGE_FIRST = 1;

			// how do these next variables ultimately figure into a link / SQL query?
			// the have to be distilled into a LIMIT clause
			$page_next = $page_current + 1; // reduce $limit_start by limit_return
			$page_previous = $page_current - 1; // increase $limit_start by limit_return

			// need to increment $page_total if it is not an integer
		   // get results


		   /*
			if user clicks "all"
				use query as is
			else
				use query concatenated with a limit clause

		   */

		    $query .= " LIMIT $sql_limit OFFSET $sql_offset";
		    $result = mysql_query($query) or die(mysql_error());
		  // display what the person searched for
		  echo "<p>Here are the results for your search for <strong> &quot;" . $search_term_raw .
		  		"&quot; </strong> (" . $numrows . " unpaginated results found)
		  </p>";

/*
		echo "<ul id=\"search_pagination\" class=\"nav_pagination\">";
		echo "<li>Showing rows $row_start to $row_end of $row_total, page $page_current of $page_total</li>";
		echo "<li><a href='index.php?p=product_search_processor&amp;search_term=$search_term_edited&amp;limit_start=$limit_start&amp;limit_return=$limit_return'>First</a></li>";

		  echo "<li>Previous</li>";
		  echo "<li>Next</li>";
		  echo "<li>Last</li>";
		  echo "<li>All</li></ul>";
*/
		  // apparently I don't need to concatenate WITHIN quotes in an echo statement
		  echo '<table class="scrollable">
					<thead>
						<tr>
							<th>Row</th>
							<th>PK</th>
							<!-- <th>Order Proposed</th>-->
							<th>Print Tag</th>
							<th>Edit</th>
							<th>Clone</th>
							<th>Archive</th>
							<th>Product Name</th>
							<th>Size</th>
							<th>Pack</th>
							<th>Vendor</th>
							<th>Landed</th>
							<th>Target</th>
							<th>Retail</th>
							<th class=\'danger\'>DANGER</th>
						</tr>
					</thead>
			<tbody>';
		  $count = 1 + $s ;

		  // now you can display the results returned
		    while ($row = mysql_fetch_array($result)) {
			    $idItemNum = $row["idItemNum"];
			    $productName = $row["productName"];
			    $size = $row["size"];
			    $pack = $row["pack"];
			    $vendor = $row["vendor"];
			    $landed = $row["landed"];
			    $cost = $row["cost"];
			    $retail = $row["retail"];

/*
<div class="comment <?php print ztog('products')?>">
 <?php print $comment_text ?>
 */
?>

	    <tr class="product <?php print ztog('products')?>" >
			<td><?php print $count ?></td>
			<td><?php print $idItemNum ?></td>
			<!-- <td><center><a href='index.php?p=flag_set_order_proposed&amp;idItemNum=$idItemNum'>Proposed</a></center></td> -->
			<td><a href='index.php?p=flag_set_print_tag&amp;idItemNum=$idItemNum'>Print Tag</a></td>
			<td><a href='index.php?p=product_edit_form&amp;idItemNum=$idItemNum'>Edit</a></td>
			<td><a href=''>Clone</a></td>
			<td><a href=''>Archive</a></td>
			<td><?php print $productName ?></td>
			<td><?php print $size ?></td>
			<td><?php print $pack ?></td>
			<td><?php print $vendor ?></td>
			<td><?php print $landed ?></td>
			<td><?php print $cost ?></td>
			<td><?php print $retail ?></td>
			<td><a href='index.php?p=drop_product&amp;idItemNum=$idItemNum'>Drop</a></td>
	    </tr>

<?php
			    // increment row count
			    $count++ ;
		    }
		  echo "</tbody></table>";
	    } // end of else block


//--------------------------------------------------------------------------------------------------

} else { // Tell them to use the search form.
	echo '<p class="error">Please use the search form at the top of the window to search this site.</p>';
}
?>