<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
    // Need the BASE_URL, defined in the config file:
    require_once ('../../config.php');
    // Redirect to the index page:
    $url = BASE_URL . 'index.php?p=archived';
    header ("Location: $url");
    exit;
}
// =============================================================================
?>

<h2>Archived (Rough Draft)</h2>

<?php

    // next line has to be included here, but can't be included in the edit module
    //mysql_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");
    $query = "select * from view_PHP_archived";

    //Set the zebra row styling variables
$row_class_odd = "row_odd";
$row_class_even = "row_even";
$row_class_css = $row_class_odd;

    // If we have no results, tell the user
        if (!$query){
            echo "<p>Sorry, we have no items to order</p>";
        }
        else { // get results
            $result = mysql_query($query) or die(mysql_error());
            // display header row
            echo '<div class="table_container">
		  <table class="scroll_table">
                    <thead class="fixed_thead"><tr>
                        <th>ProductName</th>
                        <th>Size</th>
                        <th>Cost</th>
                        <th>Retail</th>
                        <th>Rep</th>
                        </tr></thead>
			<tbody class="scroll_tbody">';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $productName = $row["productName"];
                    $size = $row["size"];
                    $cost = $row["cost"];
                    $retail = $row["retail"];
                    $rep = $row["rep"];
                    $dateAdded = $row["dateAdded"];

                    // print rows to screen
		    // for some reason, single quotes in the next echo don't work,
		    // apparently because PHP variables are involved?
                    echo "<tr class=\"$row_class_css\" >
                            <td>$productName</td>
                            <td>$size</td>
                            <td>$cost</td>
			    <td>$retail</td>
                            <td id=\"tbl_ord_rep\">$rep</td>
                            </tr>";

			//deal with the row styling
			if($row_class_css == $row_class_odd){
				$row_class_css = $row_class_even;
			} else {
				$row_class_css = $row_class_odd;
			}
                } // END WHILE

            echo "</tbody></table></div>";
        } // END ELSE
?>
