<?php
// note, some things are kinda built, but not really used as-is, might put them here


/*
combine flag setting and clearing functionality into one handler?
likewise with all numeric "updating" functionality?
*/

//------------------------------------------------------------------------------
// includes don't seem to work inside a SWITCH .....

/*
require_once ('../config_route_partials/routes_extras.php');
require_once ('../config_route_partials/routes_active.php');
require_once ('../config_route_partials/routes_temp.php');
require_once ('../config_route_partials/routes_unbuilt.php');
*/
//------------------------------------------------------------------------------

// db migration routing (temporary)
case 'db_migrate':
	$page = $route_segment_migrate . 'mysql_to_postgres_insert.php';
	$page_title = 'Migrating DB from MySQL to Postgres';
	$module_name = $page_title;
	break;

case 'dbsv':
	$page = $route_segment_db_misc . 'show_views.php';
	$page_title = 'List Database Views';
	break;

case 'db_dump':
	$page = 'db_dump.php';
	$page_title = 'DB Dump';
	break;


case 'db_import':
	$page = 'db_import.php';
	$page_title = 'DB Import';
	break;

case 'archived':
	$page = $route_segment_db_views . 'view_prod_archived.php';
	$page_title = 'Archived (Version 1)';
	break;

//------------------------------------------------------------------------------
// db views routing
	case 'vfco':
		$page = $rtsg_dbv . 'view_flag_credit_owed.php';
		$page_title = 'View Flags > Credits Owed';
		break;

	case 'vfvoos':
		$page = $rtsg_dbv . 'view_flag_voos.php';
		$page_title = 'View Flags > Vendor Out Of Stock';
		break;
	
	case 'recent':
		$page = $rtsg_dbv . 'view_prod_recent.php';
		$page_title = 'Recent Product Additions';
			// $tbl_id = 'recent';	// for jquery
		break;


	case 'order_mgt_view':
		$page = 'order_mgt_view.mod.php';
		$page_title = 'Order Management Form';
		break;

	case 'order_mgt_form':
		$page = 'order_mgt_form.mod.php';
		$page_title = 'Order Management Form';
		break;

	case 'order_mgt_processor':
		$page = 'order_mgt_processor.mod.php';
		$page_title = 'Order Management Processor';
		break;

//------------------------------------------------------------------------------
// numeric routing

	case 'update_bcf_base':
		$page = $route_segment_numeric . 'update_bcf_base.php';
		$page_title = 'Update BCF Base';
		break;

	case 'update_bcf':
		$page = $route_segment_numeric . 'update_bcf.php';
		$page_title = 'Update BCF Base';
		break;

	case 'update_osp':
		$page = $route_segment_numeric . 'update_order_subtotal_projected.php';
		$page_title = 'Update Order Projected Subtotals';
		break;

//------------------------------------------------------------------------------
// flags routing

	case 'flag_set_print_tag':
		$page = $route_segment_flags . 'flag_set_print_tag.php';
		$page_title = 'Print Tag';
		break;

	case 'flag_set_order_proposed':
		$page = $route_segment_flags . 'flag_set_order_proposed.php';
		$page_title = 'Order Liquor';
		break;

	case 'flag_clear_print':
		$page = $route_segment_flags . 'flag_clear_print.php';
		$page_title = 'Clear Print Flags';
		break;

	case 'flag_clear_order_final':
		$page = $route_segment_flags . 'flag_clear_order_final.php';
		$page_title = 'Clear Final Order Flags';
		break;

//------------------------------------------------------------------------------
// housekeeping routing

	case 'circumvent':
		$page = $rtsg_hsk . 'redirect.php';
		$page_title = 'dont be an ass';
		break;

	case 'server_info':
		$page = $rtsg_hsk . 'server_info.php';
		$page_title = 'Server Info';
		break;


//------------------------------------------------------------------------------
?>
