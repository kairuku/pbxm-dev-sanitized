<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
	// Need the BASE_URL, defined in the config file:
	require_once ('../../config.php');
	// Redirect to the index page:
	$url = BASE_URL . 'index.php?p=flag_set_order_proposed';
	header ("Location: $url");
	//echo "db inside conditional"; // this seems to get ignored if I access the script directly with a search term
	// at the same time, no echo's can come before the header statement, or the header will throw an error
	exit;
}
// =====================================================================

if (isset($_GET['idItemNum'])){
  $idItemNum = $_GET['idItemNum'];
}

$sql =  "UPDATE `1_products`
         SET `order_flag_proposed`='y'
         WHERE `idItemNum`='$idItemNum' LIMIT 1";

if (@mysql_query($sql)) {
    echo '<p>Order proposed flag set.';
  } else {
    echo '<p>Error setting order proposed flag: ' .
        mysql_error() . '</p>';
}
?>
