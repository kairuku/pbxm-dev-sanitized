<?php
// indentation, block annotation and pagination cruft cleaned up 7 january 2013

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
	// Need the BASE_URL, defined in the config file:
	require_once ('../../config.php');
	// Redirect to the index page:
	$url = BASE_URL . 'index.php?p=spn';

		// Pass along search terms?
		if (isset($_GET['search_term'])) {
			$url .= '&search_term=' . urlencode($_GET['search_term']);
		}

	header ("Location: $url");
	exit;
}
// =============================================================================

// this page expects to receive $_GET['terms']
// Display the search results if the form has been submitted.
if (isset($_GET['search_term']) && ($_GET['search_term'] != '') ) { // if isset

	// Get the search variable(S) from URL
	$search_term_raw = @$_GET['search_term'] ;
	$search_term_edited = trim($search_term_raw); //trim whitespace
	$search_term_edited = str_replace("%","\%",$search_term_edited); // escape percentage signs

	// check for an empty string and display a message.
	if ($search_term_edited == "")  {
		echo "<p>Please enter a search...</p>";
		exit;
	}

	// check for a search parameter
	if (!isset($search_term_raw))  {
	    echo "<p>We dont seem to have a search parameter!</p>";
	    exit;
	}

	// create query
	$query = "select * FROM view_search_products WHERE productName
	    		like \"%$search_term_edited%\" order by productName";
	$numresults=mysql_query($query);
	$numrows=mysql_num_rows($numresults);

	// if we have no results, tell the user
	if ($numrows == 0){ // consider reversing the order of these conditions if it helps readability
	     echo "<p>Sorry, no results matched &quot;<strong>" . $search_term_raw . "</strong>&quot;</p>";
	}
	else { // else inner
	    $result = mysql_query($query) or die(mysql_error());

		// display what the person searched for
		echo "<p>" . $numrows . " unpaginated results found</p>";

		// apparently I don't need to concatenate WITHIN quotes in an echo statement
		//echo '<table class="scrollable">
		echo '<table>
				<thead>
					<tr>
						<th>Row</th>
						<th>PK</th>
						<th>Barcode</th>
						<th>Product Name</th>
						<th>Size</th>
						<th>Pack</th>
						<th>Landed</th>
						<th>Target</th>
						<th>Retail</th>
						<th>Proposed</th>
						<th>Print Tag</th>
						<th>Clone</th>
						<th>Archive</th>
						<th>Vendor</th>
						<th>Company Temp</th>
					</tr>
				</thead>
				<tbody>';

		// now you can display the results returned
		while ($row = mysql_fetch_array($result)) {
			$idItemNum		= $row["idItemNum"];
			$productName	= $row["productName"];
			$upc			= $row["upc"];
			$size 			= $row["size"];
			$pack 			= $row["pack"];
			$vendor 		= $row["vendor"];
			$company_temp	= $row["company_temp"];
			$landed 		= $row["landed"];
			$cost 			= $row["cost"];
			$retail 		= $row["retail"];

			echo "<tr class=\"$zstate\" >
					<td>$count</td>
					<td>$idItemNum</td>
					<td>$upc</td>
					<td><a href='index.php?p=product_edit_form&amp;idItemNum=$idItemNum'>
						$productName</a></td>
					<td>$size</td>
					<td>$pack</td>
					<td>$landed</td>
					<td>$cost</td>
					<td>$retail</td>
					<td><a href='index.php?p=flag_set_order_proposed&amp;idItemNum=$idItemNum'>
						Proposed</a></td>
					<td><a href='index.php?p=flag_set_print_tag&amp;idItemNum=$idItemNum'>
						Print Tag</a></td>
					<td><a href=''>Clone</a></td>
					<td><a href=''>Archive</a></td>
					<td>$vendor</td>
					<td>$company_temp</td>
			    </tr>";

			//deal with the row styling
			if($zstate == $zebra_on){
				$zstate = $zebra_off;
			} else {
				$zstate = $zebra_on;
			}

		    $count++ ;
		    } // end of while
		  echo "</tbody></table>";
	    } // end else inner
} // end if isset

else { // Tell them to use the search form.
	echo '<p class="error">Please use the search form at the top of the window to search this site.</p>';
}
?>