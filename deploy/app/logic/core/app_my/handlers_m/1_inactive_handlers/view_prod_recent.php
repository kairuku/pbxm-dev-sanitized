<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
    // Need the BASE_URL, defined in the config file:
    require_once ('../../config.php');
    // Redirect to the index page:
    $url = BASE_URL . 'index.php?p=recent';
    header ("Location: $url");
    exit;
}
// =============================================================================

    // next line has to be included here, but can't be included in the edit module
    //mysql_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");
    $query = "select * from vuphp_recentadditions";

    // If we have no results, tell the user
        if (!$query){
            echo "<p>Sorry, we have no items to order</p>";
        }
        else { // get results

            $result = mysql_query($query) or die(mysql_error());
            // display header row
            // echo '<div class="table_container">
            echo '<div>
					<table id="recent" class="scroll_table">
                    <thead class="fixed_thead"><tr>
                        <th>ProductName</th>
                        <th>Size</th>
                        <th>Cost</th>
                        <th>Retail</th>
                        <th>Rep</th>
                        <th>Date Added</th>
                        </tr></thead>
			<tbody class="scroll_tbody">';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $productName = $row["productName"];
                    $size = $row["size"];
                    $cost = $row["cost"];
                    $retail = $row["retail"];
                    $rep = $row["rep"];
                    $dateAdded = $row["dateAdded"];

                    // print rows to screen
		    // for some reason, single quotes in the next echo don't work,
		    // apparently because PHP variables are involved?

                    // echo "<tr class=\"$zstate\" >
                    echo "<tr>
                            <td class=\"freeze\"> $productName</td>
                            <td>$size</td>
                            <td>$cost</td>
							<td>$retail</td>
                            <td id=\"tbl_ord_rep\">$rep</td>
                            <td>$dateAdded</td>
                            </tr>";

			//deal with the row styling
			if($zstate == $zebra_on){
				$zstate = $zebra_off;
			} else {
				$zstate = $zebra_on;
			}
                } // END WHILE

            echo "<tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
            </tbody></table></div>";
	    /* echo "<p>Note: scrolling table only works in Firefox, Opera, Google Chrome,
		Safari (or more generally, browsers using the Gecko, WebKit and
		Presto <a href=\"http://en.wikipedia.org/wiki/Comparison_of_layout_engines_(XHTML)\" target=\"_blank\">layout engines</a>); it doesn't work in IE (imagine that)</p>"; */
        } // END ELSE
?>
