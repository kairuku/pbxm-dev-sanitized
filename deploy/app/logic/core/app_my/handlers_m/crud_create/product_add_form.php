<?php
// compare to edit version; there is no query logic here; how to handle if forms
// become templates?
?>

<form class="data_entry" action="<?php echo FORM_ACTION; ?>" method="post">
<div class="form_right">
    <!--
    <p style="background:#7FFF00;">
        [REFACTOR ME, BIATCH] "Public" has to be YES for "Recent Additions" to be somewhat accurate.</p>
    -->

      <!-- consider having a separate QUERY PROCESSOR script, so that each form can use the same processor;
    merely call THIS include into the FORMS, as it really has nothing to do with the PROCESSING -->
    Vendor:    <select name="childVendor">
                <option value="ignore">Choose Vendor</option>
                <option value="ignore">-------------------</option>
                <?php
                    $queryVendors = "SELECT id, vendor FROM 2_vendors ORDER BY vendor";
                    $resultVendors = mysql_query($queryVendors);
                    while($vendorsArray=mysql_fetch_array($resultVendors)){
                        $childVendor=$vendorsArray[id];
                        $displayVendor=$vendorsArray[vendor];
                        echo "<option value=$childVendor>$displayVendor</option>";
                    }
                ?>
            </select>
    Size:    <select name="childSize">
                <option value="ignore">Choose Size</option>
                <option value="ignore">-------------------</option>
                <?php
                    $querySizes = "SELECT id, size FROM 2_sizes ORDER BY size";
                    $resultSizes = mysql_query($querySizes);
                    while($sizesArray=mysql_fetch_array($resultSizes)){
                        $childSize=$sizesArray[id];
                        $displaySize=$sizesArray[size];
                        echo "<option value=$childSize>$displaySize</option>";
                    }
                ?>
            </select>
    Country:    <select name="childCountry">
                <option value="ignore">Choose Country</option>
                <option value="ignore">-------------------</option>
                <?php
                    $queryCountries = "SELECT id, country FROM 2_countries ORDER BY country";
                    $resultCountries = mysql_query($queryCountries);
                    while($countriesArray=mysql_fetch_array($resultCountries)){
                        $childCountry=$countriesArray[id];
                        $displayCountry=$countriesArray[country];
                        echo "<option value=$childCountry>$displayCountry</option>";
                    }
                ?>
            </select>
            <br><br>
    <!-- ///////////////////////////////////////////////////////////////////// -->



    Product Name:<input type="text" name="productName" size="60" maxlength="255"><br>
    Company (temp):<input type="text" name="company_temp" size="60" maxlength="255"><br>
    <br>
    Cost Target:<input type="text" name="cost" size="20" maxlength="255" /><br>
    Landed:<input type="text" name="landed" size="20" maxlength="255"><br>
    Retail:<input type="text" name="retail" size="20" maxlength="255"><br>
    Case Pack:<input type="text" name="pack" size="20" maxlength="255"><br>

<hr>
    Public (deprecated?)?:&nbsp;&nbsp;&nbsp;
	   No <input type="radio" checked="checked" name="public" value="n">
	   Yes <input type="radio" name="public" value="y"><br>

<!-- ....................................................................................................................... -->
    Added While DG Gone?:&nbsp;&nbsp;&nbsp;
       No <input type="radio" checked="checked" name="flag_new_while_dg_gone" value="n">
       Yes <input type="radio" name="flag_new_while_dg_gone" value="y"><br>

<!-- ....................................................................................................................... -->

Print As: [NEW]
<hr>
    Print Tag:&nbsp;&nbsp;&nbsp;
        No <input type="radio" checked="checked" name="printTag" value="n">
        Yes <input type="radio" name="printTag" value="y"><br>



    Budget Group (may differ from taxonomy group):&nbsp;&nbsp;&nbsp;
	   Liquor <input type="radio" checked="checked" name="order_group" value="liquor">
	   Wine <input type="radio" name="order_group" value="wine"><br>

    Order By (deprecated?):&nbsp;&nbsp;&nbsp;
       Unknown <input type="radio" checked="checked" name="order_by" value="unk">
       Ignore <input type="radio" name="order_by" value="ignore">
       Bottle <input type="radio" name="order_by" value="bottle">
       Case <input type="radio" name="order_by" value="case">
       Drop <input type="radio" name="order_by" value="drop"><br>

<!--
    INSERT INTO seems to fail with INT columns??

    Order Quantity (Cases):<input type="text" name="order_qty_cs" size="20" maxlength="255" /><br>
    Order Quantity (Bottles):<input type="text" name="order_qty_bt" size="20" maxlength="255" /><br>
-->

        Order Status dropdown: [NEW]<br>

    Order Subtotal Projected:<input type="text" name="order_subtotal_projected" size="20" maxlength="255" /><br>
    Order Comments:<input type="text" name="order_comments" size="20" maxlength="255" /><br>
</div><!-- form right -->
<div class="form_left">
  <input class="button_submit_fixed" type="submit" name="prod_insert" value="+" />
  <!--<input class="button" class="button_submit_fixed" type="submit" name="prod_update" value="UPDATE" /></p>-->
  <input type="hidden" name="p" value="product_add_processor" />
  </div> <!-- form left -->
</form>