<?php
/*
  // Redirect if this page was accessed directly:
  if (!defined('BASE_URL')) {
      // Need the BASE_URL, defined in the config file:
      require_once ('../config.php');
      // Redirect to the index page:
      $url = BASE_URL . 'index.php?p=meta';
      header ("Location: $url");
      exit;
  }
*/
// =============================================================================

// you define this variable here so that it exists for the call to exec
// $output = null;
// exec('cat /etc/issue', $output);

echo "<table>
  <tbody>
    <tr>
      <td>hostname</td>
      <td>$hostn</td>
    </tr>
    <tr>
      <td>pwd</td>
      <td>" . exec('pwd') . "</td>
    </tr>
    <tr>
      <td>whoami</td>
      <td>". exec('whoami') . "</td>
    </tr>
    <tr>
      <td>machine role:</td>
      <td>$hostif</td>
    </tr>
    <tr>
      <td>lsb_release -ds</td>
      <td>" . exec('lsb_release -ds') . "</td>
    </tr>
    <tr>
      <td>uname -a</td>
      <td>" . exec('uname -a') . "</td>
    </tr>
    <tr>
      <td>uptime</td>
      <td>" . exec('uptime') . "</td>
    </tr>
    <tr>
      <td>server time</td>
      <td>" . exec('date') . "</td>
    </tr>
  </tbody></table>";

//--------------------------------------------------------------------------

// hostname conditional?
/*
  $sql = "SHOW FULL TABLES IN lcap WHERE TABLE_TYPE LIKE 'VIEW'";

  if (@mysql_query($sql)) {
    echo '<p>Current Database Views:';
  } else {
    echo '<p>Error listing views: ' .
        mysql_error() . '</p>';
  }
*/

    // next line has to be included here, but can't be included in the edit module
    //mysql_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");

  // SHOW FULL TABLES IN ibexm_geogaddii WHERE TABLE_TYPE LIKE 'VIEW';
    $query1 = "SHOW FULL TABLES IN " . $my_db_name_1 . " WHERE TABLE_TYPE LIKE 'VIEW'";
    $query5 = "SHOW FULL TABLES IN " . $my_db_name_1 ;
    $query2 = "SHOW TRIGGERS";
    $query3 = "SHOW PROCEDURE STATUS";
    $query4 = "SHOW FUNCTION STATUS";

    $error1 = "Yo, we gots no views ta show";
    $error1 = "Yo, we gots no tables ta show";
    $error2 = "Yo, we gots no triggers ta show";
    $error3 = "Yo, we gots no procedures ta show";
    $error4 = "Yo, we gots no functionz ta show<br /><br />";

// ----------------------------------------------------------------------------------

// query1
 // If we have no results, tell the user
    // this style of empty set handling is NOT useful, see other method
    // at query2 below for HTML output comparison; avoid this in all modules
        if (!$query1){
            echo $error1;
        }
        else { // get results

            $result = mysql_query($query1) or die(mysql_error());
            // display header row
            echo '<div><table>
                    <thead><tr>
                        <th>View Name</th>
                        <th>MySQL Type</th>
                        </tr></thead>
            <tbody>';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $vu = $row[0];
                    $vt = $row[1];

                    // print rows to screen
            // for some reason, single quotes in the next echo don't work,
            // apparently because PHP variables are involved?
                    echo "<tr class=\"$zebra_state\" >
                            <td>$vu</td>
                            <td>$vt</td>
                            </tr>";

                            //deal with the row styling
                            if($zebra_state == $zebra_on){
                                $zebra_state = $zebra_off;
                            } else {
                                $zebra_state = $zebra_on;
                            }
                                } // END WHILE

                            echo "</tbody></table></div>";
        } // END ELSE

//////////////////////////////////////////////////////////////////////
// query5 (out of sequence)
//////////////////////////////////////////////////////////////////////
 // If we have no results, tell the user
    // this style of empty set handling is NOT useful, see other method
    // at query2 below for HTML output comparison; avoid this in all modules
        if (!$query5){
            echo $error5;
        }
        else { // get results

            $result = mysql_query($query5) or die(mysql_error());
            // display header row
            echo '<div><table>
                    <thead><tr>
                        <th>View Name</th>
                        <th>MySQL Type</th>
                        </tr></thead>
            <tbody>';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $vu = $row[0];
                    $vt = $row[1];

                    // print rows to screen
            // for some reason, single quotes in the next echo don't work,
            // apparently because PHP variables are involved?
                    echo "<tr class=\"$zebra_state\" >
                            <td>$vu</td>
                            <td>$vt</td>
                            </tr>";

                            //deal with the row styling
                            if($zebra_state == $zebra_on){
                                $zebra_state = $zebra_off;
                            } else {
                                $zebra_state = $zebra_on;
                            }
                                } // END WHILE

                            echo "</tbody></table></div>";
        } // END ELSE

//////////////////////////////////////////////////////////////////////

// query 2
//////////////////////////////////////////////////////////////////////
    echo "<hr>";
    $numresults2=mysql_query($query2);
    $numrows2=mysql_num_rows($numresults2);

    // if we have no results, tell the user
    if ($numrows2 == 0){ // consider reversing the order of these conditions if it helps readability
         echo $error2;
    }
        else { // get results

            $result = mysql_query($query2) or die(mysql_error());
            // display header row
            echo '<div><table>
                    <thead><tr>
                        <th>Trigger Name</th>
                        <th>MySQL Type</th>
                        </tr></thead>
            <tbody>';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $vu = $row[0];
                    $vt = $row[1];

                    // print rows to screen
            // for some reason, single quotes in the next echo don't work,
            // apparently because PHP variables are involved?
                    echo "<tr class=\"$zebra_state\" >
                            <td>$vu</td>
                            <td>$vt</td>
                            </tr>";

                            //deal with the row styling
                            if($zebra_state == $zebra_on){
                                $zebra_state = $zebra_off;
                            } else {
                                $zebra_state = $zebra_on;
                            }
                                } // END WHILE

                            echo "</tbody></table></div>";
        } // END ELSE


// query 3
    echo "<hr>";
    $numresults3=mysql_query($query3);
    $numrows3=mysql_num_rows($numresults3);

    // if we have no results, tell the user
    if ($numrows3 == 0){ // consider reversing the order of these conditions if it helps readability
         echo $error3;
    }
        else { // get results

            $result = mysql_query($query3) or die(mysql_error());
            // display header row
            echo '<div><table>
                    <thead><tr>
                        <th>Procedure Name</th>
                        <th>MySQL Type</th>
                        </tr></thead>
            <tbody>';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $vu = $row[0];
                    $vt = $row[1];

                    // print rows to screen
            // for some reason, single quotes in the next echo don't work,
            // apparently because PHP variables are involved?
                    echo "<tr class=\"$zebra_state\" >
                            <td>$vu</td>
                            <td>$vt</td>
                            </tr>";

                            //deal with the row styling
                            if($zebra_state == $zebra_on){
                                $zebra_state = $zebra_off;
                            } else {
                                $zebra_state = $zebra_on;
                            }
                                } // END WHILE

                            echo "</tbody></table></div>";
        } // END ELSE


// query 4
    echo "<hr>";
    $numresults4=mysql_query($query4);
    $numrows4=mysql_num_rows($numresults4);

    // if we have no results, tell the user
    if ($numrows4 == 0){ // consider reversing the order of these conditions if it helps readability
         echo $error4;
    }
        else { // get results

            $result = mysql_query($query4) or die(mysql_error());
            // display header row
            echo '<div><table>
                    <thead><tr>
                        <th>Function Name</th>
                        <th>MySQL Type</th>
                        </tr></thead>
            <tbody>';
            // loop through content rows
                while ($row = mysql_fetch_array($result)) {
                    $vu = $row[0];
                    $vt = $row[1];

                    // print rows to screen
            // for some reason, single quotes in the next echo don't work,
            // apparently because PHP variables are involved?
                    echo "<tr class=\"$zebra_state\" >
                            <td>$vu</td>
                            <td>$vt</td>
                            </tr>";

                            //deal with the row styling
                            if($zebra_state == $zebra_on){
                                $zebra_state = $zebra_off;
                            } else {
                                $zebra_state = $zebra_on;
                            }
                                } // END WHILE

                            echo "</tbody></table></div>";
        } // END ELSE

// phpinfo();


?>