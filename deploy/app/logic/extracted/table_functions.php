<?php
// http://stackoverflow.com/questions/5661070/highlight-table-row

// variable init
$zebra_on = "zebra_on";
$zebra_off = "zebra_off";
$zstate = $zebra_on;

// now create reusable function to use the variable
// zebra_toggle($zebra_state);
/*
function ztog() {
    global $zstate;

    if($zstate == $zebra_on)
        {
            $zstate = $zebra_off;
        }
    else
        {
            $zstate = $zebra_on;
        }
  //  return $zstate;
}
*/

// http://figure-w.co.uk/zebra-striping-in-php/
function ztog($id = 'global',
                     $odd_class = 'odd', $even_class='even') {
  static $cursor = array();
  if(!isset($cursor[$id])) {
    $cursor[$id] = 1;
  }
  return $cursor[$id]++ %2? $odd_class: $even_class;
}


?>