<?php

// determine where we are, for later conditionals
$app_public_dir = getcwd();  // has largely replaced hostname for this purpose
$hostn = gethostname();

// var_dump($app_public_dir);  die;

//------------------------------------------------------------------------------
//  previous colors:
// "#B88E6A"  // sorta ecru
//------------------------------------------------------------------------------
// DEVEL
if ($app_public_dir == '/zed/www/ibex-lemp/ibexm-devel/deploy/app/public') {
  define ('URI_BASE', '/zed/www/ibex-lemp/ibexm-devel/deploy/');
  define ('URL_APP', 'http://ibexm-devel.laptop/');
  define ('URL_ASSETS', 'http://assets.ibexm-devel.laptop/');
  define ('URL_CDN', 'http://cdn.ibexm-devel.laptop/');
  // misc
  $hdbg  = "#C00000";
  $hostif = 'laptop, bare metal, development instance (zed dir)';
  $machenv = 'dev, Orion';
  // database
  $my_db_name_1 = 'ibexm_devel'; 
  $my_db_host_1 = 'localhost';
  $my_db_user_1 = 'root';
  $my_db_pass_1 = '';
}
//------------------------------------------------------------------------------
// STANDBY
elseif ($app_public_dir == '/zed/www/ibex-lemp/ibexm-standby/deploy/app/public') {
  define ('URI_BASE', '/zed/www/ibex-lemp/ibexm-standby/deploy/');
  define ('URL_APP', 'http://ibexm-standby.laptop/');
  define ('URL_ASSETS', 'http://assets.ibexm-standby.laptop/');
  define ('URL_CDN', 'http://cdn.ibexm-standby.laptop/');
  // misc
  $hdbg  = "#800000";
  $hostif = 'laptop, bare metal, standby instance (zed dir)';
  $machenv = 'standby, Orion';
  // database
  $my_db_name_1 = 'ibexm_standby'; 
  $my_db_host_1 = 'localhost';
  $my_db_user_1 = 'root';
  $my_db_pass_1 = '';
}
//------------------------------------------------------------------------------
// LYRA, FORMERLY URSA; hostname was changed on 2015 june 29
elseif ($hostn == 'lyra') {                                 // elseif ($app_public_dir == '/zed/www/ibex-lemp/ibexm-vps/deploy/app/public') {
  define ('URI_BASE', '/zed/www/ibex-lemp/ibexm-vps/deploy/');
  define ('URL_APP', 'http://ibexm-adl.qoyan.com/');        // define ('URL_APP', 'http://ibexm-ursa.qoyan.org/');
  define ('URL_ASSETS', 'http://ibexm-sdl.qoyan.com/');     // define ('URL_ASSETS', 'http://assets.ibexm-ursa.qoyan.org/');
  define ('URL_CDN', 'http://ibexm-scdl.qoyan.com/');       // define ('URL_CDN', 'http://cdn.ibexm-ursa.qoyan.org/');
  // misc
  $hdbg  = "#000"; // black
  $hostif = 'app on DO VPS running Arch, quasi-production for now, but may be relegated to db import when printing is needed';
  $machenv = 'Arch on Lyra (was Ursa)';
  // database
  $my_db_name_1 = 'SOME_DB'; // hyphen causes mysql error, so still using underscore
  $my_db_host_1 = 'localhost';
  $my_db_user_1 = 'UNKNOWN';
  $my_db_pass_1 = 'SECRET';

  // do not create VIEWS in remote db, IP address gets tied to DEFINER
  // server IP:
  // xxx.xxx.xxx.xxx

  // home IP:
  // CREATE USER 'user'@'xxx.xxx.xxx.xxx' IDENTIFIED BY 'SECRET';
  // GRANT ALL PRIVILEGES ON *.* TO 'user'@'xxx.xxx.xxx.xxx' IDENTIFIED BY 'SECRET';
  // FLUSH PRIVILEGES;

  // work IP:
  // CREATE USER 'user'@'xxx.xxx.xxx.xxx' IDENTIFIED BY 'SECRET';
  // GRANT ALL PRIVILEGES ON *.* TO 'user'@'xxx.xxx.xxx.xxx' IDENTIFIED BY 'SECRET';
  // FLUSH PRIVILEGES;
}
//------------------------------------------------------------------------------
else {
  echo "no servers here!";
}

//------------------------------------------------------------------------------
// be sure the domains are set in /etc/hosts on the HOST os, not the guest
// get away from relying on "localhost"

// NEW, SIMPLIFIED REDIRECTION PARADIGM
// USE ABSOLUTE PATH, PREDICATED ON USING SAME USER ON EVERY *NIX MACHINE

// COPY THIS TO THE TOP OF EACH NEW PHP FILE
/*
// =============================================================================
// Redirect if this page was accessed directly:
if (!defined('APP_BASE_URL')) {
    // Need the redirection URL, defined in the paths file:
    require_once ('../../config/paths.php');

    // Redirect to the index page:
    $redirect_url = APP_BASE_URL . REDIRECT_PATH;

    header ("Location: $redirect_url");
    exit;
}
// =============================================================================
*/

//‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡
// GENERAL CONSTANT DEFINITIONS
//‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡‡

define ('URI_CODE', URI_BASE . 'app/');

define ('URI_INCLUDES', URI_CODE . 'logic/core/includes/');
define ('URI_STATUS', URI_INCLUDES . 'ibexm_status.inc.php');
define ('URI_NOTES', URI_INCLUDES . 'ibexm_notes.inc.php');

define ('URI_INCLUDES_M', URI_CODE . 'logic/core/app_my/includes_m/');
define ('URI_TOTALS_M', URI_INCLUDES_M . 'order_totals.inc.php');
define ('URI_TABLES_M', URI_INCLUDES_M . 'product_tables_common.inc.php');

define ('URI_HANDLERS_M', URI_CODE . 'logic/core/app_my/handlers_m/');

define ('URI_TEMPLATE_GLOBAL', URI_CODE . 'ui/templating/global/');
define ('URI_TEMPLATE_LOCAL', URI_CODE . 'ui/templating/local/');

// require_once (URI_TEMPLATE_LOCAL . 'product-edit-form/product-edit-form-main.html');


// replaces index.php; edit "index" in nginx vhost file
// consider reconciling with an nginx rewrite rule
define ('FRONT_CONTROLLER', 'fc.php');
// need to reconcile this with hard-coded links in the product edit form (delete record)
// and product_tables_common.inc.php (product name column as edit record link)

// replaces index.php
define ('FORM_ACTION', './fc.php');

//------------------------------------------------------------------------------

// create a ~base~ view merely to handle joins, don't exclude rows in a WHERE clause?
// then have other views pull from this view with unique WHERE clauses?
// would I still want an SQL constant then?
// define ('PRODUCT_SELECT_QUERY', 'SELECT * FROM 1_products');

//------------------------------------------------------------------------------
// MySQL-oriented routing path segments

define ('SEG_M_HSK', URI_HANDLERS_M . '0_housekeeping/');
define ('SEG_M_OTHER', URI_HANDLERS_M . 'other_handlers/');
define ('SEG_M_CRUD_CREATE', URI_HANDLERS_M . 'crud_create/');
define ('SEG_M_CRUD_READ', URI_HANDLERS_M . 'crud_read/');
define ('SEG_M_CRUD_UPDATE', URI_HANDLERS_M . 'crud_update/');
define ('SEG_M_CRUD_DELETE', URI_HANDLERS_M . 'crud_delete/');

// Postgres-oriented router path ~segments~



?>
