<?php

// determine which page is requested
if (isset($_GET['p'])) { // e.g., links
  $p = $_GET['p'];
} elseif (isset($_POST['p'])) { // e.g, forms
  $p = $_POST['p'];
} else {
  $p = NULL;
}
//------------------------------------------------------------------------------

// determine which handler to invoke
switch ($p) {
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CRUD read routes
  case 'spn':
      $search_suffix = $_GET['search_term'];
    $page = SEG_M_CRUD_READ . 'search_product_name.php';
    $page_title = 'Product Name Search Results for "' . $search_suffix .'"';
    break;
  case 'spb':
      $search_suffix = $_GET['search_code'];
    $page = SEG_M_CRUD_READ . 'search_product_barcode.php';
    $page_title = 'Barcode Search Results for "' . $search_suffix .'"';
    break;
// the final order page is also the default/home page
  case 'vocf':
    $page = SEG_M_CRUD_READ . 'view_order_combined_final.php';
    $page_title = 'Order Combined, Final';
    break;
  case 'voleg':
    $page = SEG_M_CRUD_READ . 'view_order_combined_legacy.php';
    $page_title = 'Order Combined, Legacy';
    break;
  case 'vocp':
    $page = SEG_M_CRUD_READ . 'view_order_combined_proposed.php';
    $page_title = 'Order Combined, Proposed';
    break;
  case 'vfofe':
    $page = SEG_M_CRUD_READ . 'view_flag_order_final_error.php';
    $page_title = 'View Flags > Order Final > Error';
    break;

  case 'vfptg':
    $page = SEG_M_CRUD_READ . 'view_flag_print_tag.php';
    $page_title = 'View Flags Print Tags';
    break;

// new viewers as of 2014 oct

  case 'vfptk':
    $page = SEG_M_CRUD_READ . 'view_flag_print_talker.php';
    $page_title = 'View Flags Print Talkers';
    break;

  case 'vfmkh':
    $page = SEG_M_CRUD_READ . 'view_flag_has_markdown_tag.php';
    $page_title = 'View Flags Has Markdown Tag';
    break;

  case 'vfmkp':
    $page = SEG_M_CRUD_READ . 'view_flag_pre_markdown.php';
    $page_title = 'View Flags Pre-Markdown List';
    break;

  case 'vfmk':
    $page = SEG_M_CRUD_READ . 'view_flag_markdown.php';
    $page_title = 'View Flags Marked Down';
    break;

  case 'vfclr':
    $page = SEG_M_CRUD_READ . 'view_flag_clearance.php';
    $page_title = 'View Flags Clearance Items';
    break;

  case 'vfvoos':
    $page = SEG_M_CRUD_READ . 'view_flag_voos.php';
    $page_title = 'View Flags Vendor Out of Stock';
    break;

  case 'vftks':
    $page = SEG_M_CRUD_READ . 'view_flag_tarik_seeks.php';
    $page_title = 'View Flags Tarik Seeks!';
    break;


// ----------------------------------------------------

  case 'valc':
    $page = SEG_M_CRUD_READ . 'view_flag_allocated.php';
    $page_title = 'View Flag Allocated';
    break;
  case 'vnwdg':
    $page = SEG_M_CRUD_READ . 'view_flag_new_while_dg_gone.php';
    $page_title = 'View Items Added While DG Was Gone';
    break;
  case 'vfpno':
    $page = SEG_M_CRUD_READ . 'view_flag_public_no.php';
    $page_title = 'View Items Where Public Is No';
    break;
  case 'vfobu':
    $page = SEG_M_CRUD_READ . 'view_flag_order_by_unk.php';
    $page_title = 'View Items Order By Unknown or Ignore';
    break;


// misc views can go here;
// add entry to the menu, and copy, e.g., the allocated view file and modify

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// introduce POST success/fail vars here for non-SELECT queries
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CRUD create routes
  case 'product_add_form':
    $page = SEG_M_CRUD_CREATE . 'product_add_form.php';
    $page_title = 'Product Addition Form';
    break;
  case 'product_add_processor':
    $page = SEG_M_CRUD_CREATE . 'product_add_submit.php';
    $page_title = 'Product Addition Results';
    $post_success = 'New product added.';
    $post_fail = 'Error adding product:<br>';
    break;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CRUD update routes (with forms)
  case 'product_edit_form':
    $page = SEG_M_CRUD_UPDATE . 'product_edit_form.php';
      $idItemNum = $_GET['idItemNum'];
      $title_query = sqlw_query("SELECT `productName`,`size`
               FROM `1_products`, `2_sizes`
               WHERE `idItemNum`='$idItemNum'
               AND `1_products`.`childSize` = `2_sizes`.`id`");
      $title_fetch = sqlw_fetch_array($title_query);
      // how about adding similar functionality to edit and addition ~processor~ handlers
    $page_title = 'Product Editing Form For [' . $title_fetch['size'] . '] ' . $title_fetch['productName'];
    break;
  case 'product_edit_processor':
    $page = SEG_M_CRUD_UPDATE . 'product_edit_submit.php';
    $page_title = 'Product Editing Results';
    $post_success = 'Product details updated.<br>(Should possibly return to prev search results here.)';
    $post_fail = 'Error updating product details:<br>';
    break;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CRUD update routes (no forms)
  case 'flag_clear_print':
    $page = SEG_M_CRUD_UPDATE . 'flag_clear_print.php';
    $page_title = 'Clear Print Flags';
    break;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CRUD delete routes
  case 'prod_del':
    $page = SEG_M_CRUD_DELETE . 'product_delete.php';
    $page_title = 'Product Dropped';
    break;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// other app routes
  case 'db_dump':
    $page = SEG_M_OTHER . 'db_dump.php';
    $page_title = 'Database Dump Generated';
    break;
  case 'db_import':
    $page = SEG_M_OTHER . 'db_import.php';
    $page_title = 'Dump File Imported Into Database';
    break;
  case 'apnfo':
    $page = SEG_M_OTHER . 'app_info.php';
    $page_title = 'App Info';
    break;
/*
  case 'db_conv':
    $page = SEG_M_OTHER . 'db_conv.php';
    $page_title = 'Database Conversion Temp';
    break;
*/
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Default is to include the order page
  default:
    $page = SEG_M_CRUD_READ . 'view_order_combined_final.php';
    $page_title = 'Order Combined, Final';
    break;
} // End of main switch 

//------------------------------------------------------------------------------

// housekeeping routes

// determine if the chosen handler exists
  // THIS PROBABLY NO LONGER WORKS IN THE "RECENT ADDITIONS" ERA
// if (!file_exists('../logic/core/' . $page)) {
if (!file_exists($page)) {
  $page = SEG_M_HSK . 'no_exist.php';
  $page_title = 'Existential fail';
}

//------------------------------------------------------------------------------

// USE THE FOLLOWING PARADIGM CONSISTENTLY FOR INTRA-PAGE SUB-TEMPLATE INCLUDES

// Include the global page header file:
// require_once ('../ui/templating/global/header-global.html');
require_once (URI_TEMPLATE_GLOBAL . 'header-global.html');
require_once (URI_TEMPLATE_GLOBAL . 'nav-menu-global.html');

// Include the content-specific handler determined by he above switch:
require_once ($page);

// Include the global page footer file to complete the template:
require_once (URI_TEMPLATE_GLOBAL . 'footer-global.html');

?>
