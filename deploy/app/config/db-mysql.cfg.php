<?php

// amended 20 Feb 2014
//------------------------------------------------------------------------------
// http://stackoverflow.com/questions/1544541/how-to-connect-to-multiple-databases-in-a-single-php-page

$my_db_handle_1 = mysql_connect($my_db_host_1, $my_db_user_1, $my_db_pass_1);
mysql_select_db($my_db_name_1, $my_db_handle_1) or die("Unable to select database");

// this line is (was?) ESSENTIAL ~derrick
// see the htaccess file for the other half of the unicode solution
//update: find the webpage that mentioned the htaccess cuz i have no idea what i meant
mysql_query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");
// more info: http://www.hotscripts.com/forums/php/22364-set-names-utf8.html


// -----------------------------------------------------------------------------
// shell_exec("mysqldump --user=root --password=password --result-file=\"/home/ubu/aphex_arb.sql\" lcap");

// note the double quotes below, single quotes fail
$my_dump_string_1 = "mysqldump --user=$my_db_user_1 --password=$my_db_pass_1 $my_db_name_1 > $my_dump_file_1";

function my_dump_1 ($my_dump_string_1) {
	exec($my_dump_string_1);
}

//------------------------------------------------------------------------------
// this will now be more complex because of including xtal
// the links will now need a page bit, a to db bit, and a from db bit
// for now the link is disabled, revert to the command line

function my_import ($plat_set) {
// requires the write bit to be set on the path, on EVERY machine, e.g., chmod -R 0777 Dropbox/mach_all/db_dumps
	if ($plat_set == 'afxd'){		// afx devel
		exec('mysql --user=root --password=password lcap < /home/ubu/Dropbox/mach_all/db_dumps/geog_ibexm_arb.sql');
	}
	elseif ($plat_set == 'afxf'){	// afx fallback
		exec('mysql --user=root --password=SECRET ibexm_af_fb < /home/ubu/Dropbox/mach_all/db_dumps/geog_ibexm_arb.sql');
	}
	elseif ($plat_set == 'xtal'){	// xtal
		exec('mysql --user=root --password=SECRET ibexm_xtal < /home/ubu/Dropbox/mach_all/db_dumps/geog_ibexm_arb.sql');	
	}
	else { // geogaddi
		exec('mysql --user=ibex_dba --password=SECRET ibexm_geogaddi < /home/ubu/Dropbox/mach_all/db_dumps/ibexm_fbk_aphex_arb.sql');
	}
} // end my_dump definition


// -----------------------------------------------------------------------------

// DATABASE FUNCTION WRAPPER LIBRARY HERE
// SWAP OUT POSTGRES AND MYSQL AS NEEDED
// SWAP OUT UPDATED PHP LIBRARY FUNCTIONS IN GENERAL AS NEEDED

function sqlw_query ($query_arg) {
	$query_result = mysql_query($query_arg);
	return $query_result;
}

function sqlw_fetch_array ($fetch_arg) {
	$fetch_result = mysql_fetch_array($fetch_arg);
	return $fetch_result;
}

// -----------------------------------------------------------------------------
// postgres
// sudo aptitude install php5-pgsql
// sudo /etc/init.d/php5-fpm restart
// $conn_string = "host=localhost dbname=ibex_dev user=ubu password=password";

// $conn = pg_connect($conn_string) or die('connection failed');
// $conn = pg_connect($conn_string);

?>
