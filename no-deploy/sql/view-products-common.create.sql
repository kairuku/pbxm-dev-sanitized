-- ORIGINALLY:
-- /abode/anspace/servers/assemble/dba/db-sql-source/ibexm-sql/sql-actual-mysql/views/2014-current/PROTO-2014/


-- this is intended to be a generic, READ-ONLY view; individual PHP scripts can
-- pull from this view with different WHERE clauses in the PHP script itself
-- this common exists mainly for JOINS
-- add any columns desired for individual scripts to THIS common view and update
-- the common table HTML template accordingly

-- beware of using AS ...

CREATE OR REPLACE VIEW
view_products_common
    AS SELECT
-- =============================================================================
1_products.idItemNum,
1_products.productName,
1_products.upc,
1_products.selector_ff,
-- =============================================================================
1_products.order_qty_bt,
1_products.order_qty_cs,
1_products.order_by,
CONCAT(FORMAT(order_subtotal_projected, 2)) AS order_subtotal_projected,
CONCAT(FORMAT(cost, 2)) AS cost,
CONCAT(FORMAT(landed, 2)) AS landed,
-- concat('$',format(1_products.retail,2)) AS `retail`,
CONCAT(FORMAT(retail, 2)) AS retail,
-- CONCAT(FORMAT(bcf_base, 2)) AS bcf_base,
-- CONCAT(FORMAT(bcf, 2)) AS bcf,
1_products.pack,
1_products.company_temp,
1_products.orderComments,
-- 1_products.order_group,
-- =============================================================================
1_products.printTag,
1_products.public,
1_products.flag_order_legacy,
1_products.flag_new_while_dg_gone,

-- 2014 October additions
1_products.allocated,
1_products.tarik_seeks,
1_products.voos,
1_products.pre_markdown,
1_products.markdown,
1_products.clearance,
1_products.has_markdown_tag,
1_products.printTalker,

-- =============================================================================
1_products.childSize,
2_sizes.size,
-- =============================================================================
1_products.childOrderStatus,
2_order_status.order_status_abbreviation AS status,
-- =============================================================================
1_products.childDeal,
2_deals.deal,
-- =============================================================================
1_products.childVendor,
2_vendors.vendor,
2_vendors.`rep-full-name`,
2_vendors.`rep-phone`
-- =============================================================================
    FROM
1_products,
2_deals,
2_sizes,
2_vendors,
2_order_status
-- =============================================================================
    WHERE
1_products.childDeal = 2_deals.id
AND 1_products.childSize = 2_sizes.id
AND 1_products.childOrderStatus = 2_order_status.id
AND 1_products.childVendor = 2_vendors.id
-- =============================================================================
    ORDER BY
1_products.productName ASC,
2_sizes.size ASC
;
