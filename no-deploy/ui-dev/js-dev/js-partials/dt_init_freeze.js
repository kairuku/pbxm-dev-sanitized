$(document).ready( function () {
    var oTable = $('#liqfin').dataTable( {
        "sScrollY": "550px",
        "sScrollX": "100%",
        "sScrollXInner": "250%",
        "bScrollCollapse": true,
        "bPaginate": false
    } );
    new FixedColumns( oTable );
} );
