<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
	// Need the BASE_URL, defined in the config file:
	require_once ('../../config.php');
	// Redirect to the index page:
	$url = BASE_URL . 'index.php?p=update_bcf';
  header ("Location: $url");
	//echo "db inside conditional"; // this seems to get ignored if I access the script directly with a search term
	// at the same time, no echo's can come before the header statement, or the header will throw an error
	exit;
}
// =============================================================================

$sql= "UPDATE 1_products
      SET bcf=bcf_base/pack
      WHERE bcf_base=14.4
      AND pack > 0"
      ;

if (@mysql_query($sql)) {
    echo '<p>BCF computed values updated.</p>';
  } else {
    echo '<p>Error updating BCF computed values: ' .
        mysql_error() . '</p>';
}
?>
