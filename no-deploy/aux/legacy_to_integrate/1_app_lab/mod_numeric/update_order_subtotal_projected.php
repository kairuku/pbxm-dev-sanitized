<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
	// Need the BASE_URL, defined in the config file:
	require_once ('../../config.php');
	// Redirect to the index page:
	$url = BASE_URL . 'index.php?p=update_osp';
  header ("Location: $url");
	exit;
}
// =============================================================================

// http://stackoverflow.com/questions/6734231/mysql-update-case-help

$sql= "UPDATE 1_products
      SET order_subtotal_projected=
        CASE order_by
          WHEN 'bottle' THEN ((cost+bcf)*order_qty_proposed)
          WHEN 'case' THEN ((cost*pack)*order_qty_proposed)
          WHEN 'drop' THEN ((cost*pack)*order_qty_proposed)
          WHEN 'ignore' THEN order_subtotal_projected
          ELSE 0
        END
        WHERE `order_flag_final`='y'"
      ;

if (@mysql_query($sql)) {
    echo '<p>Order projected subtotals updated.</p>';
  } else {
    echo '<p>Error updating order projected subtotals: ' .
        mysql_error() . '</p>';
}


// http://forums.phpfreaks.com/topic/215924-mysql-how-to-use-update-with-if-else-condition/
// http://www.terminally-incoherent.com/blog/2009/10/05/mysql-conditional-update/
// http://stackoverflow.com/questions/2177543/mysql-using-if-then-else-in-mysql-update-or-select-queries


/*
        IF (order_by='unk') THEN
            UPDATE 1_products
            SET order_subtotal_projected=0
            WHERE idItemNum=idItemNum;
        ELSEIF (NEW.order_by='bottle') THEN
            UPDATE 1_products
            SET order_subtotal_projected=((cost+bcf)*order_qty_proposed)
            WHERE idItemNum=idItemNum;
        ELSEIF (NEW.order_by='case') THEN
            UPDATE 1_products
            SET order_subtotal_projected=((cost*pack)*order_qty_proposed)
            WHERE idItemNum=idItemNum;
*/

/*
        UPDATE elec_products
        SET stats = CASE programmename
         WHEN 'Argos' THEN 1
         WHEN 'stify' THEN 2
         ELSE 3
        END CASE
        WHERE programmename IS NOT NULL
*/



/*

        UPDATE mytable
        SET foo= IF(foo IS NULL, 'bar', foo)
        WHERE id='69'

        UPDATE mytable
        SET
           foo= IF(foo IS NULL, '1', foo),
           bar = '2',
           baz = IF(baz > '10', 'high', 'low')
        WHERE id='69'


UPDATE table
SET A = IF(A > 0 AND A < 1, 1, IF(A > 1 order_by='unk'AND A < 2, 2, A))
WHERE A IS NOT NULL;
*/

?>
