<?php
// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {
	// Need the BASE_URL, defined in the config file:
	require_once ('../../config.php');
	// Redirect to the index page:
	$url = BASE_URL . 'index.php?p=update_bcf_base';
  header ("Location: $url");
	//echo "db inside conditional"; // this seems to get ignored if I access the script directly with a search term
	// at the same time, no echo's can come before the header statement, or the header will throw an error
	exit;
}
// =============================================================================

        /*
        $sql = "UPDATE `1_products` SET
                  `printTag`='n',
                  `printTalker`='n',
                  `printTasting`='n'";
        */

$sql= "UPDATE 1_products
      SET bcf_base=14.4
      WHERE childVendor=7
      OR childVendor=40
      OR childVendor=34
      OR childVendor=50
      OR childVendor=36
      OR childVendor=39
      OR childVendor=43
      OR childVendor=6
      OR childVendor=31
      OR childVendor=47
      OR childVendor=51
      OR childVendor=32
      OR childVendor=24
      OR childVendor=41
      OR childVendor=42
      OR childVendor=33"
      ;

if (@mysql_query($sql)) {
    echo '<p>BCF base values updated.</p>';
  } else {
    echo '<p>Error updating BCF base values: ' .
        mysql_error() . '</p>';
}
?>
