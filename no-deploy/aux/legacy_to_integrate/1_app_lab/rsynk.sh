RSYNC=/usr/bin/rsync
ARGS="-aiv --delete --progress --stats"

APHEX_SRC=/home/ubu/Dropbox/machines/aphex_dev/php_public/
XTAL_DEST=/home/ubu/Dropbox/machines/xtal/php_public/

# won't work with vostro because that dropbox direction is not inside this vm;
# have to run that script from the host machine

$RSYNC $ARGS $APHEX_SRC $XTAL_DEST
