<?php # Script ___________________

/* 
 *	This is the ____________ module.
 *	This page is included by index.php.
  */

// Redirect if this page was accessed directly:
if (!defined('BASE_URL')) {

	// Need the BASE_URL, defined in the config file:
	require_once ('../includes/config.inc.php');
	require_once(DB_MYSQL_ADMIN); // last chance, if the template was circumvented
		
	// Redirect to the index page:
	$url = BASE_URL . 'index.php?p=order_product';
	
	header ("Location: $url");
	//echo "db inside conditional"; // this seems to get ignored if I access the script directly with a search term
	// at the same time, no echo's can come before the header statement, or the header will throw an error
	exit;
	
} // End of defined() IF.

// **********************************************************


if (isset($_GET['idItemNum'])){
  $idItemNum = $_GET['idItemNum'];
}
  
  $sql = "UPDATE `1_products`
          SET `order`='y' 
          WHERE `idItemNum`='$idItemNum' LIMIT 1";
  
  if (@mysql_query($sql)) {
    echo '<p>Order flag set.';
  } else {
    echo '<p>Error setting order flag: ' .
        mysql_error() . '</p>';
  }
?>